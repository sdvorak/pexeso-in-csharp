﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Pexeso.Pexeso
{
    public enum ACTIONS { NEW_GAME, MAIN_MENU, EXIT_ALL }

    public class Program
    {
        public static List<Form> openedForms = new List<Form>();
        public static ACTIONS action;

        public static MainMenu mm;
        public static Pexeso p;

        public static Timer myTimer = new Timer();

        public static void CloseAllWindows()
        {
            for (int i = 0; i < openedForms.Count; ++i)
            {
                openedForms[i].Close();
                openedForms.Remove(openedForms[i]);
            }
        }

        [STAThread]
        public static void Main()
        {
            /*
             * Pexeso
             */
            //Application.Run(new Pexeso(18));
            //Application.Run(new Pexeso(8));

            Program.action = ACTIONS.MAIN_MENU;

            while (action != ACTIONS.EXIT_ALL)
            {
                switch (action)
                {
                    case ACTIONS.MAIN_MENU:
                        action = ACTIONS.EXIT_ALL;
                        CloseAllWindows();
                        mm = new MainMenu();
                        openedForms.Add(mm);
                        Application.Run(mm);
                        break;
                    case ACTIONS.NEW_GAME:
                        action = ACTIONS.EXIT_ALL;
                        CloseAllWindows();
                        p = new Pexeso(8);
                        openedForms.Add(p);
                        Application.Run(p);
                        break;
                }
            }
        }
    }
}
