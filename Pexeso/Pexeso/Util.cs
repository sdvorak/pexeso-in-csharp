﻿using System;
using System.Collections.Generic;

namespace Pexeso
{
    public static class Util
    {
        private static Random rng = new Random();

        public static List<T> RandomizeList<T>(List<T> list)
        {
            int listCount = list.Count;
            for (int i = 0; i < listCount; ++i)
            {
                int src = i;
                int dest = rng.Next(listCount);
                T temp = list[dest];
                list[dest] = list[src];
                list[src] = temp;
            }

            return list;
        }

        public static List<int> CreatePieces(int size)
        {
            List<int> pieces = new List<int>();
            for (int i = 1; i < size + 1; ++i)
            {
                // add two times
                pieces.Add(i);
                pieces.Add(i);
            }
            return pieces;
        }
    }
}
