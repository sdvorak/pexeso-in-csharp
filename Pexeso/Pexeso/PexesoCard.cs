﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Pexeso.Pexeso
{
    public class PexesoCard : Control
    {
        private int value;
        private PictureBox pictureBox;
        private Label secondSide;

        private static bool sleepFlag = false;

        private static List<PexesoCard> shownPieces;

        public PexesoCard(int value)
        {
            this.value = value;

            shownPieces = new List<PexesoCard>();

            // init picturebox
            pictureBox = new PictureBox();
            pictureBox.Size = new Size(50, 50);
            pictureBox.BackgroundImage = Image.FromFile("../../Pexeso/img/abstract-image.png");
            pictureBox.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBox.Visible = true;
            pictureBox.Click += new EventHandler(this.PexesoPictureBoxClicked);

            // init label
            secondSide = new Label();
            secondSide.Text = this.value.ToString();
            secondSide.Width = 50;
            secondSide.Height = 50;
            secondSide.Font = new Font("Arial", 30);
            secondSide.TextAlign = ContentAlignment.MiddleCenter;
        }

        public object GetInitialObject()
        {
            return this.pictureBox;
        }

        private static void SetSide(PexesoCard pc, bool flipped)
        {
            Control current = flipped ? (Control)pc.pictureBox : (Control)pc.secondSide;
            Control next = flipped ? (Control)pc.secondSide : (Control)pc.pictureBox;

            int r = Pexeso.pex.GetRow(current);
            int c = Pexeso.pex.GetColumn(current);

            Pexeso.pex.Controls.Remove(current);
            Pexeso.pex.Controls.Add(next, c, r);
        }

        private static void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            Program.myTimer.Stop();

            sleepFlag = false;

            foreach (PexesoCard pc in shownPieces)
                SetSide(pc, false);

            shownPieces = new List<PexesoCard>();
        }


        public void PexesoPictureBoxClicked(object sender, EventArgs e)
        {
            if (sleepFlag) return;

            PexesoCard.shownPieces.Add(this);

            bool allSame = AllSame(PexesoCard.shownPieces);

            if (PexesoCard.shownPieces.Count == 2 && !allSame)
            {
                sleepFlag = true;
                Program.myTimer.Tick += new EventHandler(TimerEventProcessor);

                // Sets the timer interval to 1.5 seconds.
                Program.myTimer.Interval = 1500;
                Program.myTimer.Start();
            }
            else if (PexesoCard.shownPieces.Count == 2 && allSame)
            {
                PexesoCard.shownPieces = new List<PexesoCard>();
                Pexeso.score += 1;
                Pexeso.SetLabelScore(Pexeso.score);
                Pexeso.scoreLabel.Invalidate();
                Pexeso.scoreLabel.Update();
            }


            if (Pexeso.score == Pexeso.targetScore)
            {
                Pexeso.score = 0;
                Program.action = ACTIONS.MAIN_MENU;
                Program.p.Close();
            }


            SetSide(this, true);
        }

        private bool AllSame(List<PexesoCard> list)
        {
            for (int i = 0; i < PexesoCard.shownPieces.Count - 1; ++i)
                if (PexesoCard.shownPieces[i].value != PexesoCard.shownPieces[i + 1].value) return false;
            return true;
        }
    }
}
