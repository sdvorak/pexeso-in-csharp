﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Pexeso.Pexeso
{
    public class Pexeso : Form
    {
        private readonly int pexesoPairs;
        private readonly int pieceCount;
        private readonly int size;
        private List<int> pieces;

        public static int score = 0;
        public static int targetScore;

        public static TableLayoutPanel root;
        public static TableLayoutPanel pex;
        public static Label scoreLabel;

        public Pexeso(int pexesoPairs)
        {
            this.pexesoPairs = pexesoPairs;
            targetScore = this.pexesoPairs;
            this.pieceCount = pexesoPairs * 2;

            this.size = (int)Math.Ceiling(Math.Sqrt(pieceCount));

            pieces = Util.CreatePieces(this.pexesoPairs);
            pieces = Util.RandomizeList(pieces);

            this.InitComponent();
        }

        private void InitComponent()
        {
            this.Text = "Pexeso Game";

            pex = new TableLayoutPanel();
            pex.RowCount = this.size;
            pex.ColumnCount = this.size;
            pex.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            pex.Anchor = AnchorStyles.None;
            pex.AutoSize = true;

            int curRowNumber = 0;
            int curColumnNumber = 0;
            for (int i = 0; i < this.pieces.Count; ++i)
            {
                if (i % size == 0 && i != 0) ++curRowNumber;
                curColumnNumber = i % size;
                PexesoCard cur = new PexesoCard(pieces[i]);
                pex.Controls.Add((PictureBox)cur.GetInitialObject(), curColumnNumber, curRowNumber);
            }

            FlowLayoutPanel header = new FlowLayoutPanel();
            scoreLabel = new Label();
            scoreLabel.Font = new Font("Arial", 25);
            scoreLabel.AutoSize = true;
            Pexeso.SetLabelScore(0);

            header.Controls.Add(scoreLabel);
            this.Controls.Add(header);
            this.Controls.Add(pex);

            this.ClientSize = new Size(550, 450);
            this.CenterToScreen();
        }

        public static void SetLabelScore(int score)
        {
            scoreLabel.Text = "Score: " + score.ToString();
        }
    }
}
